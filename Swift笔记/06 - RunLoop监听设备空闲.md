```swift
func runLoop() {
    
    let keyDict = [
        NSNumber(value: 1): "kCFRunLoopEntry",
        NSNumber(value: 2): "kCFRunLoopBeforeTimers",
        NSNumber(value: 4): "kCFRunLoopBeforeSources",
        NSNumber(value: 32): "kCFRunLoopBeforeWaiting",
        NSNumber(value: 64): "kCFRunLoopAfterWaiting",
        NSNumber(value: 128): "kCFRunLoopExit"
    ]
    
    let flags: CFRunLoopActivity = [.entry, .beforeTimers, .beforeSources, .beforeWaiting, .afterWaiting, .exit, .allActivities]
    
    let runloopObserver = CFRunLoopObserverCreateWithHandler (
        kCFAllocatorDefault,
        flags.rawValue,
        true,
        CFIndex(0),
        { observer, activity in
            /*
             看到最多的是 这样的循环
             runloop == kCFRunLoopAfterWaiting
             runloop == kCFRunLoopBeforeTimers
             runloop == kCFRunLoopBeforeSources
             runloop == kCFRunLoopBeforeWaiting
             当任务全部完成，将要waiting时，会发送一个kCFRunLoopBeforeWaiting通知
             所以，我们知道RunLoop将空闲时，会发送 kCFRunLoopBeforeWaiting 通知， 告诉我们 他将要进入 waiting 状态。
             */
            printLog("runLoop == \(keyDict[NSNumber(value: activity.rawValue)] ?? "")")
            //self.perform(#selector(self.test), with: nil, afterDelay: 2, inModes: [.default])
        })
    
    CFRunLoopAddObserver(CFRunLoopGetCurrent(), runloopObserver, .defaultMode)
}
```

