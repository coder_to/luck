https://github.com/realm/SwiftLint/blob/master/README_CN.md

### 使用 [Homebrew](http://brew.sh/)：

```
brew install swiftlint
```

### Xcode

整合 SwiftLint 到 Xcode 体系中去从而可以使警告和错误显示到 IDE 上，只需要在 Xcode 中添加一个新的“Run Script Phase”并且包含如下代码即可：

```
if which swiftlint >/dev/null; then
  swiftlint
else
  echo "warning: SwiftLint not installed, download from https://github.com/realm/SwiftLint"
fi
```

忽略cocoapods第三方库代码规范问题，在项目的根目录下新建一个名为 `.swiftlint.yml` 的配置文件，输入如下内容：

```undefined
excluded: 
  - Pods
```