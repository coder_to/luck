1 value为0，阻塞当前线程，线程等待
2.value为1，当成锁来使用，串行执行
3.value大于1，限制线程生成数量

```swift
let semaphore = DispatchSemaphore(value: 0)
printLog("1")
DispatchQueue.global(qos: .default).async {
    printLog("2")
  	//信号量+1
    semaphore.signal()
}
//信号量-1 如果>0，则向下执行，否则等待
semaphore.wait()
printLog("3")

#打印
1
2
3
(lldb) 
```

