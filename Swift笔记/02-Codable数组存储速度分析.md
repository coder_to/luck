先说结论，同样测试两次

PropertyListEncoder、PropertyListDecoder归档和解档两次耗时18和21、16和20

JSONEncoder、JSONDecoder归档和解档两次耗时14和16、13和17

大概得出JSONEncoder和JSONDecoder速度更快。

同样适用BGFMDB这个框架，sqlite存储，使用用一样的模型结构去做对比，发现框架内部模型转数据很久，无奈停止了。

然后适用数组，内部是字符串的简单数据，存储和获取数据时间分别是 16和7 16和13 16和24，对比下来可能系统的存储速度会更快。

```swift
struct ChapterModel: Codable {
    var title: String?
    var notes: [NoteModel] = []
}

struct NoteModel: Codable {
    var content: String?
}

//解档和归档方法
static func encode(chapters: [ChapterModel], key: String? = nil) -> Bool {
    if let url = documentURL?.appendingPathComponent("\(key ?? "ChapterModel").data"),
       let data = try? JSONEncoder().encode(chapters) {
        let reuslt = NSKeyedArchiver.archiveRootObject(data, toFile: url.path)
        return reuslt
    }
    return false
}

static func decodeArray(key: String? = nil) -> [ChapterModel]? {
    if let url = documentURL?.appendingPathComponent("\(key ?? "ChapterModel").data"){
        guard let data = NSKeyedUnarchiver.unarchiveObject(withFile: url.path) as? Data else {
            return nil
        }
        do {
            let objcect = try JSONDecoder().decode([ChapterModel].self, from: data)
            return objcect
        } catch {
            printLog("======= 解档失败")
        }
    }
    return nil
}
```

```swift
//构造1百万条数据分析
var chapters: [ChapterModel] = []
for index in 0...1000000 {
    var chapter = ChapterModel()
    chapter.title = "===\(index)"
    var item1 = NoteModel()
    item1.content = "===内容1"
    var item2 = NoteModel()
    item2.content = "===内容2"
    chapter.notes.append(contentsOf: [item1, item2])
    chapters.append(chapter)
}
//归档
ChapterModel.encode(chapters: chapters, key: nil)
//解档
_ = ChapterModel.decodeArray()
```

