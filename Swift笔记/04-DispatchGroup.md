```swift
DispatchQueue.global().async {[weak self] in
  let group = DispatchGroup()
  for book in (self?.datas ?? []) as [MJBookModel] {
      group.enter()
      book.requestDetaiPriority(false) { (isSuccess) in
          if isSuccess == true {
              book.bg_saveOrUpdateAsync(nil)
          }
          group.leave()
      }
  }

  group.notify(queue: DispatchQueue.main) {
      //所有任务
  }
}
```

https://www.jianshu.com/p/1136bc60f185

