### Terms of Service for Professional Edition

Welcome to the subscription service for the Professional Edition. You should read and comply with the "Terms of Service for Professional Edition Subscription" (hereinafter referred to as "these terms"). Your use and related actions will be deemed as acceptance of these service terms and agreement to be bound by them.

##### 1. Subscription Service Content
After activating the Professional Edition, you can use all premium features of the Professional Edition without any ads during the service period.

(a) Monthly, Quarterly, and Annual Auto-Renewal

1. The monthly, quarterly (3 months), and annual subscription plans are auto-renewal services.  
2. The prices for these products are based on the actual prices at the time of purchase. Payments will be charged in your local currency according to the exchange rates defined by iTunes at the time of purchase, and the payment will be deducted from your iTunes account upon confirmation of purchase.

(b) Monthly, Quarterly, and Annual Subscription Services

1. If you opt for the monthly, quarterly, or annual subscription service, you authorize PureLibro to automatically deduct the Professional Edition service fee for the next billing period from your iTunes account (referred to as "the account") before the expiration of your current subscription. The deduction will be made within 24 hours prior to the next billing cycle. If the deduction fails due to insufficient balance in the account, PureLibro reserves the right to suspend or terminate your Professional Edition service without prior notice.  
2. To cancel the auto-renewal service, go to "Settings" → "iTunes & App Store" → select "Apple ID" → click "View Apple ID" → on the account settings page, click "Subscriptions" → cancel the subscription.

##### 2. Rights and Restrictions for Professional Edition Users

1. During the validity period of the Professional Edition service, users enjoy all the value-added services provided by PureLibro.
2. The Professional Edition service is limited to personal use only; users are prohibited from gifting, lending, using, transferring, or selling the service to others. If violated, PureLibro reserves the right to cancel the Professional Edition service for the transferred account without prior notice, and any resulting losses will be borne by the user.
3. Users are prohibited from acquiring or purchasing the Professional Edition service through illegal means such as theft, exploiting system loopholes, or purchasing from unauthorized websites selling PureLibro services. If violated, PureLibro reserves the right to cancel the user’s service qualification. Any issues arising from such violations are the responsibility of the user, and PureLibro bears no responsibility.
4. If a user repeatedly violates these terms or relevant national laws and regulations, or harms the reputation and interests of PureLibro or others, PureLibro reserves the right to revoke the user's Professional Edition service without compensation.

##### 3. Breach of Contract

If you violate these terms or any other service agreements, causing third-party complaints or lawsuits, you are solely responsible for all legal liabilities. If your illegal or breach of conduct causes loss to PureLibro, PureLibro has the right to recover the lost amount, and you shall bear the breach/tort liability and fully compensate for all losses suffered by PureLibro.

##### 4. Miscellaneous

Your use of this software indicates that you have read and agreed to be bound by these terms. PureLibro reserves the right to modify these terms when necessary. You can review the latest terms in the latest version of the software. If you continue to use the software after any changes to these terms, it will be deemed as acceptance of the modified terms. If you do not accept the modified terms, you should stop using the software.