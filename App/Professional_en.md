#### Ad free

Don't bother with ads

#### iCould Backup

Sync library, font files, reading themes, etc.

#### Reading Statistics

See how long you've read, set your reading goals, and more.

#### Custom Reading Themes

Customize background images, font colors, and create theme plans. (Comic and pdf files are not supported)

#### Set the regularity of the chapter division of the txt file

Replaces the default regular expression, and can add more regular expressions.

#### progress display

In addition to the in-chapter progress display in the free version, the professional version adds a full-text progress display.

#### Import external fonts

In addition to the rich built-in fonts, you can also import your favorite font files.

#### Double row display

It works better on iPad or in landscape orientation.

#### Export Notes

You can choose txt, html, markdown export.

#### Comic mode

In addition to supporting epub, mobi, azw3, azw4, azw graphic reading, it can also support comic mode (requires manual settings).

Support cbz, cbr comics (if the comic format is a compressed package of other formats, you can change the suffix to these two and then import).

#### Safe Mode

Use password or Face ID to open books or set app lock.

#### hidden mode

Save your private books, drop down the library list to turn on hidden mode with password or Face ID.

#### Library supports unlimited grouping

#### In addition to the basic functions, the development of more professional version functions will be supported in the future.