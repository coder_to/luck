 **Support txt, epub, mobi, azw3, azw4, azw, pdf, cbz, cbr, zip, rar.**

#### To import an e-book in other applications, you can follow these steps:

1. Use the file app or document manager to locate the e-book file to import and select it as the current operation.
2. Open the share menu and select the `PureLibro` application.
3. The `PureLibro` application will automatically open and display the imported e-book.

Alternatively, you can use applications such as `wifi transfer book` to upload e-books from your computer to your device.

Note that the `PureLibro` application only supports files without DRM protection, so if your e-book is DRM-protected, it cannot be opened in the application.

<img src="https://p.ipic.vip/18qbqi.png" style="zoom:50%;" />

<img src="https://p.ipic.vip/34z2ic.jpg" style="zoom:52%;" />